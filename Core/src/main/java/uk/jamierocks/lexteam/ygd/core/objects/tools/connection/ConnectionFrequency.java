package uk.jamierocks.lexteam.ygd.core.objects.tools.connection;

/**
 * An enum representing the different frequencies of connection.
 * Currently just different colours.
 *
 * @author Tom Drever
 */
public enum ConnectionFrequency {
    RED, BLUE, GREEN, ORANGE, PURPLE
}
